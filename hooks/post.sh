#!/bin/sh

if drush status --fields=bootstrap | grep -q 'Successful'; then

    echo "--> Cleaning caches..."
    drush -y cr

    echo "--> Updating database..."
    drush -y updb

    echo "--> Importing configuration..."
    drush -y cim

    echo "--> Updating entity schemas..."
    drush -y entup
fi

echo "--> Running..."